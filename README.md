# merge-env

Very simple tool to merge global and environment-specific configuration.
Environment variable with the same name as key has precedence over value in passed objects.

## Installation

`npm install git+https://bitbucket.org/teamhunch/merge-env.git --save`

## Example

```
var mergeEnv = require('merge-env');

var globalConfig = {
  SOME_SERVICE_TOKEN: false
};

var envConfig = {
  development: {
    SOME_SERVICE_TOKEN: 'devKey'
  },
  production: {
    SOME_SERVICE_TOKEN: 'prodKey'
  }
};

var config = mergeEnv(globalConfig, envConfig);
config.SOME_SERVICE_TOKEN //depend on NODE_ENV or process.env.SOME_SERVICE_TOKEN
```
