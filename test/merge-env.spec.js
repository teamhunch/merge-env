var chai = require('chai');
var expect = chai.expect;

var mergeEnv = require('../');

describe('merge-env', function () {

    var originalEnv, globalConfig;

    beforeEach(function () {
        originalEnv = process.env;
        process.env = {};

        globalConfig = {
            SOME_SERVICE_TOKEN: 'blabla',
            PORT: 80
        };
    });

    afterEach(function () {
        process.env = originalEnv;
    });

    it('should return global config when there are no env-specific values', function () {
        var config = mergeEnv(globalConfig);

        expect(config).to.deep.equal(globalConfig);
    });

    it('should return merge with development when no process.env.NODE_ENV is set', function () {
        var envConfig = {
            development: {
                SOME_SERVICE_TOKEN: 'devKey'
            }
        };

        var config = mergeEnv(globalConfig, envConfig);

        expect(config.PORT).to.equal(80);
        expect(config.SOME_SERVICE_TOKEN).to.equal('devKey');
    });

    it('should return merge with process.env.NODE_ENV key when set', function () {
        var envConfig = {
            development: {
                SOME_SERVICE_TOKEN: 'devKey'
            },
            production: {
                SOME_SERVICE_TOKEN: 'prodKey'
            }
        };

        process.env.NODE_ENV = 'production';

        var config = mergeEnv(globalConfig, envConfig);

        expect(config.PORT).to.equal(80);
        expect(config.SOME_SERVICE_TOKEN).to.equal('prodKey');
    });

    it('should all to override with env variable', function () {
        var envConfig = {
            development: {
                SOME_SERVICE_TOKEN: 'devKey'
            }
        };

        process.env.SOME_SERVICE_TOKEN = 'absolutelyNewValue';

        var config = mergeEnv(globalConfig, envConfig);

        expect(config.PORT).to.equal(80);
        expect(config.SOME_SERVICE_TOKEN).to.equal('absolutelyNewValue');
    });
});
