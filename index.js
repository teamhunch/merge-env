var extend = require('extend');
var each = require('for-each');

function mergeWithGlobal(globalConfig, config) {
    return extend({}, globalConfig, config);
}

function mergeWithProcessEnv(config) {
    each(config, function(value, key) {
        config[key] = process.env[key] || value;
    });

    return config;
}

module.exports = function (globalConfig, envConfig) {
    envConfig = envConfig || {};

    var config = mergeWithGlobal(globalConfig, envConfig[process.env.NODE_ENV || 'development']);
    return mergeWithProcessEnv(config);
};

